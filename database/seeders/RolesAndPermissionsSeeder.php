<?php

namespace Database\Seeders;

use App\Models\Role;
use App\Models\User;
use App\Models\Permission;
use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class RolesAndPermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        app()[\Spatie\Permission\PermissionRegistrar::class]->forgetCachedPermissions();

        // ------- MODULE PERMISSIONS PER MODULE ----------// 

        // ROLE MODEL
        $rolePermission1 = Permission::create(['name' => 'create_role', 'description' => 'create role']);
        $rolePermission2 = Permission::create(['name' => 'read_role', 'description' => 'read role']);
        $rolePermission3 = Permission::create(['name' => 'update_role', 'description' => 'update role']);
        $rolePermission4 = Permission::create(['name' => 'delete_role', 'description' => 'delete role']);

        // PERMISSION MODEL
        $permission1 = Permission::create(['name' => 'create_permission', 'description' => 'create permission']);
        $permission2 = Permission::create(['name' => 'read_permission', 'description' => 'read permission']);
        $permission3 = Permission::create(['name' => 'update_permission', 'description' => 'update permission']);
        $permission4 = Permission::create(['name' => 'delete_permission', 'description' => 'delete permission']);

        // USER ADMINS MODEL
        $adminPermission1 = Permission::create(['name' => 'create_admin', 'description' => 'read admin']);
        $adminPermission2 = Permission::create(['name' => 'read_admin', 'description' => 'read admin']);
        $adminPermission3 = Permission::create(['name' => 'update_admin', 'description' => 'update admin']);
        $adminPermission4 = Permission::create(['name' => 'delete_admin', 'description' => 'delete admin']);

        // USER CASHIER MODEL
        $cashierPermission1 = Permission::create(['name' => 'create_cashier', 'description' => 'create cashier']);
        $cashierPermission2 = Permission::create(['name' => 'read_cashier', 'description' => 'read cashier']);
        $cashierPermission3 = Permission::create(['name' => 'update_cashier', 'description' => 'update cashier']);
        $cashierPermission4 = Permission::create(['name' => 'delete_cashier', 'description' => 'delete cashier']);

        // CATEGORY MODULE
        $categoryPermission1 = Permission::create(['name' => 'create_category', 'description' => 'read category']);
        $categoryPermission2 = Permission::create(['name' => 'read_category', 'description' => 'read category']);
        $categoryPermission3 = Permission::create(['name' => 'update_category', 'description' => 'update category']);
        $categoryPermission4 = Permission::create(['name' => 'delete_category', 'description' => 'delete category']);

        // TABLE MODULE
        $tablePermission1 = Permission::create(['name' => 'create_table', 'description' => 'read table']);
        $tablePermission2 = Permission::create(['name' => 'read_table', 'description' => 'read table']);
        $tablePermission3 = Permission::create(['name' => 'update_table', 'description' => 'update table']);
        $tablePermission4 = Permission::create(['name' => 'delete_table', 'description' => 'delete table']);

        // MENU MODULE
        $menuPermission1 = Permission::create(['name' => 'create_menu', 'description' => 'read menu']);
        $menuPermission2 = Permission::create(['name' => 'read_menu', 'description' => 'read menu']);
        $menuPermission3 = Permission::create(['name' => 'update_menu', 'description' => 'update menu']);
        $menuPermission4 = Permission::create(['name' => 'delete_menu', 'description' => 'delete menu']);

        // ORDER MODULE
        $orderPermission1 = Permission::create(['name' => 'create_order', 'description' => 'read order']);
        $orderPermission2 = Permission::create(['name' => 'read_order', 'description' => 'read order']);
        $orderPermission3 = Permission::create(['name' => 'update_order', 'description' => 'update order']);
        $orderPermission4 = Permission::create(['name' => 'delete_order', 'description' => 'delete order']);

        // Misc
        $miscPermission = Permission::create(['name' => 'N/A', 'description' => 'N/A']);

        // --  ROLE
        $superAdminRole = Role::create(['name' => 'super-admin']); // 1
        $adminRole = Role::create(['name' => 'admin']); // 2
        $cashierRole = Role::create(['name' => 'cashier']); // 3
        $waiterRole = Role::create(['name' => 'waiter']); // 4

        // .. Role Sync Permission
        $superAdminRole->syncPermissions([
            $rolePermission1,
            $rolePermission2,
            $rolePermission3,
            $rolePermission4,
            $permission1,
            $permission2,
            $permission3,
            $permission4,
            $adminPermission1,
            $adminPermission2,
            $adminPermission3,
            $adminPermission4,
            $categoryPermission1,
            $categoryPermission2,
            $categoryPermission3,
            $categoryPermission4,
            $tablePermission1,
            $tablePermission2,
            $tablePermission3,
            $tablePermission4,
            $cashierPermission1,
            $cashierPermission2,
            $cashierPermission3,
            $cashierPermission4,
            $menuPermission1,
            $menuPermission2,
            $menuPermission3,
            $menuPermission4,
            $orderPermission1,
            $orderPermission2,
            $orderPermission3,
            $orderPermission4,
        ]);

        $adminRole->syncPermissions([
            $rolePermission1,
            $rolePermission2,
            $rolePermission3,
            $rolePermission4,
            $permission1,
            $permission2,
            $permission3,
            $permission4,
            $adminPermission1,
            $adminPermission2,
            $adminPermission3,
            $adminPermission4,
            $categoryPermission1,
            $categoryPermission2,
            $categoryPermission3,
            $categoryPermission4,
            $tablePermission1,
            $tablePermission2,
            $tablePermission3,
            $tablePermission4,
            $cashierPermission1,
            $cashierPermission2,
            $cashierPermission3,
            $cashierPermission4,
            $menuPermission1,
            $menuPermission2,
            $menuPermission3,
            $menuPermission4,
            $orderPermission1,
            $orderPermission2,
            $orderPermission3,
            $orderPermission4,
        ]);

        $cashierRole->syncPermissions([
            $categoryPermission1,
            $categoryPermission2,
            $categoryPermission3,
            $categoryPermission4,
            $tablePermission1,
            $tablePermission2,
            $tablePermission3,
            $tablePermission4,
            $menuPermission1,
            $menuPermission2,
            $menuPermission3,
            $menuPermission4,
            $orderPermission1,
            $orderPermission2,
            $orderPermission3,
            $orderPermission4,
        ]);

        $waiterRole->syncPermissions([
            $categoryPermission2,
            $tablePermission2,
            $menuPermission2,
            $orderPermission1,
            $orderPermission2,
            $orderPermission3,
            $orderPermission4,
        ]);
        
        // .. End Role Sync Permission ..

        // Create User Admin 1
        $superAdmin = User::create([
            'name' => "super admin",
            'email' => "superadmin@sow.com",
            'password' => bcrypt("password"),
            'is_admin' => true,
            'email_verified_at' => date("Y-m-d H:i:s"),
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),
        ]); // 1

        // Create User Admin 2
        $admin = User::create([
            'name' => 'admin',
            'is_admin' => true,
            'email' => 'admin@sow.com',
            'password' => bcrypt("password"),
            'email_verified_at' => date("Y-m-d H:i:s"),
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),
        ]); // 2

        // Create User Cashier
        $cashier = User::create([
            'name' => 'cashier',
            'is_admin' => false,
            'email' => 'cashier@sow.com',
            'password' => bcrypt("password"),
            'email_verified_at' => date("Y-m-d H:i:s"),
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),
        ]); // 3

        // Create User Waiter
        $waiter = User::create([
            'name' => 'Waiter',
            'is_admin' => false,
            'email' => 'waiter@sow.com',
            'password' => bcrypt("password"),
            'email_verified_at' => date("Y-m-d H:i:s"),
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),
        ]); // 3

        // .. User Sync Role and Permissions
        $superAdmin->syncRoles([$superAdminRole])->syncPermissions([
            $rolePermission1,
            $rolePermission2,
            $rolePermission3,
            $rolePermission4,
            $permission1,
            $permission2,
            $permission3,
            $permission4,
            $adminPermission1,
            $adminPermission2,
            $adminPermission3,
            $adminPermission4,
            $categoryPermission1,
            $categoryPermission2,
            $categoryPermission3,
            $categoryPermission4,
            $tablePermission1,
            $tablePermission2,
            $tablePermission3,
            $tablePermission4,
            $cashierPermission1,
            $cashierPermission2,
            $cashierPermission3,
            $cashierPermission4,
            $menuPermission1,
            $menuPermission2,
            $menuPermission3,
            $menuPermission4,
        ]);

        $admin->syncRoles([$adminRole])->syncPermissions([
            $rolePermission1,
            $rolePermission2,
            $rolePermission3,
            $rolePermission4,
            $permission1,
            $permission2,
            $permission3,
            $permission4,
            $adminPermission1,
            $adminPermission2,
            $adminPermission3,
            $adminPermission4,
            $categoryPermission1,
            $categoryPermission2,
            $categoryPermission3,
            $categoryPermission4,
            $tablePermission1,
            $tablePermission2,
            $tablePermission3,
            $tablePermission4,
            $cashierPermission1,
            $cashierPermission2,
            $cashierPermission3,
            $cashierPermission4,
            $menuPermission1,
            $menuPermission2,
            $menuPermission3,
            $menuPermission4,
        ]);

        $cashier->syncRoles($cashierRole)->syncPermissions([
            $categoryPermission1,
            $categoryPermission2,
            $categoryPermission3,
            $categoryPermission4,
            $tablePermission1,
            $tablePermission2,
            $tablePermission3,
            $tablePermission4,
            $menuPermission1,
            $menuPermission2,
            $menuPermission3,
            $menuPermission4,
        ]);

        $waiter->syncRoles($waiterRole)->syncPermissions([
            $categoryPermission2,
            $tablePermission2,
            $menuPermission2,
        ]);

        // .. End User Sync Role and Permissions
    }
}
