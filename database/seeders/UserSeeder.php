<?php

namespace Database\Seeders;

use App\Models\Permission;
use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 1; $i <= 666; $i++) {

            $user = User::create([
                'name' => 'User Customer ' . $i,
                'email' => 'user.customer' . $i . '@gmail.com',
                'password' => bcrypt("password"),
                'is_admin' => false,
                'email_verified_at' => date("Y-m-d H:i:s"),
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s"),
            ]);

            $role = Role::where('id', 5)->first();
            // $permission = Permission::where('name', 'N/A')->first();
            // $user->syncRoles($role)->syncPermissions($permission);
            // $permission = Permission::where('name', 'N/A')->first();
            $user->syncRoles($role);
        }
    }
}
