<?php

namespace Database\Seeders;

use App\Imports\VillagesImport;
use Illuminate\Database\Seeder;
use Maatwebsite\Excel\Facades\Excel;

class VillagesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Excel::import(new VillagesImport, database_path('seeders/csvs/villages.csv'), null, \Maatwebsite\Excel\Excel::CSV);
    }
}
