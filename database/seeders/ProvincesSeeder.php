<?php

namespace Database\Seeders;

use App\Imports\ProvinceImport;
use Illuminate\Database\Seeder;
use Maatwebsite\Excel\Facades\Excel;

class ProvincesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Excel::import(new ProvinceImport, database_path('seeders/csvs/provinces.csv'), null, \Maatwebsite\Excel\Excel::CSV);
    }
}
