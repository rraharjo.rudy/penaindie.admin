<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserAddressTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_address', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id')->unsigned()->index();
            $table->foreign('user_id')->references('id')->on('users');
            $table->string('fullname');
            $table->string('firstname');
            $table->string('lastname');
            $table->string('mobile_phone');
            $table->string('photo')->nullable();
            $table->string('current_address');
            $table->string('permanent_address')->nullable();
            $table->string('postal_code');
            $table->boolean('default')->default(false);
            $table->unsignedBigInteger('province_id')->unsigned()->index();
            $table->foreign('province_id')->references('id')->on('provinces');
            $table->unsignedBigInteger('city_id')->unsigned()->index()->nullable();
            $table->foreign('city_id')->references('id')->on('cities');
            $table->unsignedBigInteger('district_id')->unsigned()->index()->nullable();
            $table->foreign('district_id')->references('id')->on('districts');
            $table->unsignedBigInteger('village_id')->unsigned()->index()->nullable();
            $table->foreign('village_id')->references('id')->on('villages');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_address');
    }
}
