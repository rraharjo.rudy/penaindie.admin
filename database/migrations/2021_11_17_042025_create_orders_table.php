<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            
            $table->unsignedBigInteger('user_id')->unsigned()->index();
            $table->foreign('user_id')->references('id')->on('users');

            $table->unsignedBigInteger('table_id')->unsigned()->index();
            $table->foreign('table_id')->references('id')->on('tables');
            
            $table->string('no_order')->nullable();
            $table->string('customer_name')->nullable();
            $table->string('customer_phone')->nullable();
            $table->string('notes')->nullable();
            $table->string('type_payment')->nullable()->comment('Cash, Transfer, Digital Payment');
            $table->double('ppn_value');
            $table->double('charge_value');
            $table->double('payment_value');
            $table->double('change_value');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
