<?php

use App\Http\Controllers\AuthorController;
use App\Http\Controllers\CashierController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\MemberController;
use App\Http\Controllers\MenuController;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\PermissionController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\TableController;
use App\Http\Controllers\UserController;
use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return Inertia::render('Welcome', [
        'canLogin' => Route::has('login'),
        'canRegister' => Route::has('register'),
        'laravelVersion' => Application::VERSION,
        'phpVersion' => PHP_VERSION,
    ]);
});

Route::group(['prefix' => 'app'], function () {

    require __DIR__ . '/auth.php';

    // Admin Routes
    Route::group(['middleware' => ['auth', 'verified', 'role:super-admin|admin|cashier|waiter']], function () {

        Route::get('/dashboard', function () {
            return Inertia::render('Admin/Dashboard/Index');
        })->name('dashboard');

        Route::group(['prefix' => 'cashier'], function () {
            Route::get('/', [CashierController::class, 'index'])->name('cashier')->middleware('permission:read_cashier');
            Route::get('/create', [CashierController::class, 'create'])->name('cashier.create')->middleware('permission:create_cashier');
            Route::post('/store', [CashierController::class, 'store'])->name('cashier.store')->middleware('permission:create_cashier');
            Route::get('/edit/{cashier}/detail', [CashierController::class, 'edit'])->name('cashier.edit')->middleware('permission:update_cashier');
            Route::put('/update/{cashier}', [CashierController::class, 'update'])->name('cashier.update')->middleware('permission:update_cashier');
            Route::delete('/{cashier}/destroy', [CashierController::class, 'destroy'])->name('cashier.destroy')->middleware('permission:delete_cashier');
            Route::get('/{id}/restore', [CashierController::class, 'restore'])->name('cashier.restore')->middleware('permission:delete_cashier');
        });

        Route::group(['prefix' => 'category'], function () {
            Route::get('/', [CategoryController::class, 'index'])->name('category')->middleware('permission:read_category');
            Route::get('/create', [CategoryController::class, 'create'])->name('category.create')->middleware('permission:create_category');
            Route::post('/store', [CategoryController::class, 'store'])->name('category.store')->middleware('permission:create_category');
            Route::get('/edit/{category}/detail', [CategoryController::class, 'edit'])->name('category.edit')->middleware('permission:update_category');
            Route::put('/update/{category}', [CategoryController::class, 'update'])->name('category.update')->middleware('permission:update_category');
            Route::delete('/{category}/destroy', [CategoryController::class, 'destroy'])->name('category.destroy')->middleware('permission:delete_category');
            Route::get('/{id}/restore', [CategoryController::class, 'restore'])->name('category.restore')->middleware('permission:delete_category');
        });

        Route::group(['prefix' => 'table'], function () {
            Route::get('/', [TableController::class, 'index'])->name('table')->middleware('permission:read_table');
            Route::get('/create', [TableController::class, 'create'])->name('table.create')->middleware('permission:create_table');
            Route::post('/store', [TableController::class, 'store'])->name('table.store')->middleware('permission:create_table');
            Route::get('/edit/{table}/detail', [TableController::class, 'edit'])->name('table.edit')->middleware('permission:update_table');
            Route::put('/update/{table}', [TableController::class, 'update'])->name('table.update')->middleware('permission:update_table');
            Route::delete('/{table}/destroy', [TableController::class, 'destroy'])->name('table.destroy')->middleware('permission:delete_table');
            Route::get('/{id}/restore', [TableController::class, 'restore'])->name('table.restore')->middleware('permission:delete_table');
        });

        Route::group(['prefix' => 'menu'], function () {
            Route::get('/', [MenuController::class, 'index'])->name('menu')->middleware('permission:read_menu');
            Route::get('/create', [MenuController::class, 'create'])->name('menu.create')->middleware('permission:create_menu');
            Route::post('/store', [MenuController::class, 'store'])->name('menu.store')->middleware('permission:create_menu');
            Route::get('/edit/{menu}/detail', [MenuController::class, 'edit'])->name('menu.edit')->middleware('permission:update_menu');
            Route::put('/update/{menu}', [MenuController::class, 'update'])->name('menu.update')->middleware('permission:update_menu');
            Route::delete('/{menu}/destroy', [MenuController::class, 'destroy'])->name('menu.destroy')->middleware('permission:delete_menu');
            Route::get('/{id}/restore', [MenuController::class, 'restore'])->name('menu.restore')->middleware('permission:delete_menu');
        });

        Route::group(['prefix' => 'order'], function () {
            Route::get('/', [OrderController::class, 'index'])->name('order')->middleware('permission:read_order');
            Route::get('/create', [OrderController::class, 'create'])->name('order.create')->middleware('permission:create_order');
            Route::post('/store', [OrderController::class, 'store'])->name('order.store')->middleware('permission:create_menu');
            Route::get('/detail/{id}', [OrderController::class, 'detail'])->name('order.detail')->middleware('permission:create_menu');
            Route::get('/invoice/{id}', [OrderController::class, 'invoice'])->name('order.invoice')->middleware('permission:create_menu');
            // Route::get('/edit/{menu}/detail', [MenuController::class, 'edit'])->name('menu.edit')->middleware('permission:update_menu');
            // Route::put('/update/{menu}', [MenuController::class, 'update'])->name('menu.update')->middleware('permission:update_menu');
            // Route::delete('/{menu}/destroy', [MenuController::class, 'destroy'])->name('menu.destroy')->middleware('permission:delete_menu');
            // Route::get('/{id}/restore', [MenuController::class, 'restore'])->name('menu.restore')->middleware('permission:delete_menu');
        });

        Route::group(['prefix' => 'profile'], function () {
            Route::get('/', [ProfileController::class, 'index'])->name('profile');
            Route::post('/store', [ProfileController::class, 'store'])->name('profile.store');
        });

        Route::group(['prefix' => 'permission'], function () {
            Route::get('/', [PermissionController::class, 'index'])->name('permission')->middleware('permission:read_permission');
            Route::get('/create', [PermissionController::class, 'create'])->name('permission.create')->middleware('permission:create_permission');
            Route::post('/store', [PermissionController::class, 'store'])->name('permission.store')->middleware('permission:create_permission');
            Route::get('/edit/{permission}/detail', [PermissionController::class, 'edit'])->name('permission.edit')->middleware('permission:update_role');
            Route::put('/update/{permission}', [PermissionController::class, 'update'])->name('permission.update')->middleware('permission:update_permission');
            Route::delete('/{permission}/destroy', [PermissionController::class, 'destroy'])->name('permission.destroy')->middleware('permission:delete_permission');
        });

        Route::group(['prefix' => 'role'], function () {
            Route::get('/', [RoleController::class, 'index'])->name('role')->middleware('permission:read_role');
            Route::get('/create', [RoleController::class, 'create'])->name('role.create')->middleware('permission:create_role');
            Route::post('/store', [RoleController::class, 'store'])->name('role.store')->middleware('permission:create_role');
            Route::get('/edit/{role}/detail', [RoleController::class, 'edit'])->name('role.edit')->middleware('permission:update_role');
            Route::put('/update/{role}', [RoleController::class, 'update'])->name('role.update')->middleware('permission:update_role');
            Route::delete('/{role}/destroy', [RoleController::class, 'destroy'])->name('role.destroy')->middleware('permission:delete_role');
        });

        Route::group(['prefix' => 'user'], function () {
            Route::get('/', [UserController::class, 'index'])->name('user')->middleware('permission:read_admin');
            Route::get('/create', [UserController::class, 'create'])->name('user.create')->middleware('permission:create_admin');
            Route::post('/store', [UserController::class, 'store'])->name('user.store')->middleware('permission:create_admin');
            Route::get('/edit/{user}/detail', [UserController::class, 'edit'])->name('user.edit')->middleware('permission:update_admin');
            Route::put('/update/{user}', [UserController::class, 'update'])->name('user.update')->middleware('permission:update_admin');
            Route::delete('/{user}/destroy', [UserController::class, 'destroy'])->name('user.destroy')->middleware('permission:delete_admin');
        });
    });
});
