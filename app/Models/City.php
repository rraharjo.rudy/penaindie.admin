<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    use HasFactory;

    protected $table = "cities";
    protected $primaryKey = "id";
    protected $fillable = [
        'id', 'province_id', 'name'
    ];

    public function scopeFilter($query, array $filters)
    {
        $query->when($filters['province_id'] ?? null, function ($query, $province_id) {
            $query->where('province_id', $province_id);
        });
    }
}
