<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class Address extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'user_address';
    protected $primaryKey = "id";

    protected $fillable = [
        'user_id',
        'fullname',
        'firstname',
        'lastname',
        'mobile_phone',
        'current_address',
        'permanent_address',
        'postal_code',
        'default',
        'province_id',
        'city_id',
        'district_id',
        'village_id',
        'photo',
        'created_at'
    ];

    protected $casts = [
        'created_at' => 'datetime:Y-m-d H:i:s',
        'updated_at' => 'datetime:Y-m-d H:i:s'
    ];

    public function province()
    {
        return $this->hasOne(Province::class, 'id', 'province_id')->select(['id', 'name']);
    }

    public function city()
    {
        return $this->hasOne(City::class, 'id', 'city_id')->select(['id', 'name']);
    }

    public function district()
    {
        return $this->hasOne(District::class, 'id', 'district_id')->select(['id', 'name']);
    }

    public function village()
    {
        return $this->hasOne('App\Models\Village', 'id', 'village_id')->select(['id', 'name']);
    }

    public function getCreatedAtAttribute($value)
    {
        $this->attributes['created_at'] = with(new Carbon($value))->diffForHumans();
    }
}
