<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Menu extends Model
{
    use HasFactory, SoftDeletes;

    public function scopeFilter($query, array $filters)
    {

        if (count($filters) == 0) {
            $filters["verify"] = "all";
        }

        
        $query->when($filters['search'] ?? null, function ($query, $search) {
            $query->where(function ($query) use ($search) {
                $query->where('name', 'like', '%' . $search . '%')
                    ->orWhere('description', 'like', '%' . $search . '%');
            });
        })->when($filters['category'] ?? null, function ($query, $category) {
            // dd($category);
            if ($category !== 'all') {
                $query->where('category_id', $category);
            }

        })->when($filters['verify'] ?? null, function ($query, $verify) {

            if ($verify === 'all') {
                $query->withTrashed();
            } elseif ($verify === 'onlyTrashed') {
                $query->onlyTrashed();
            }
        });
    }

    // public function category()
    // {
    //     return $this->hasOne(Category::class)->select([
    //         "id",
    //         "name",
    //     ]);
    // }

    public function category()
    {
        return $this->hasOne(Category::class, 'id', 'menu_id')->select(['id', 'name']);
    }
}
