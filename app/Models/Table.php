<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Table extends Model
{
    use  HasFactory, SoftDeletes;

    protected $fillable = [
        'name',
        'icon',
        'slug',
        'description',
    ];

    public function scopeFilter($query, array $filters)
    {
        if (count($filters) == 0) {
            $filters["verify"] = "all";
        }
        $query->when($filters['search'] ?? null, function ($query, $search) {
            $query->where(function ($query) use ($search) {
                $query->where('name', 'like', '%' . $search . '%');
            });
        })->when($filters['verify'] ?? null, function ($query, $verify) {

            if ($verify === 'all') {
                $query->withTrashed();
            } elseif ($verify === 'onlyTrashed') {
                $query->onlyTrashed();
            }
        });
    }
}
