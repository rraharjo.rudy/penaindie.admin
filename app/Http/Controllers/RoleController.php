<?php

namespace App\Http\Controllers;

use App\Models\Permission;
use App\Models\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Inertia\Inertia;

class RoleController extends Controller
{
    public function index()
    {
        return Inertia::render('Admin/Role/Index', [
            'roles' => Role::with('permissions')->paginate(),
        ]);
    }

    public function create()
    {
        return Inertia::render('Admin/Role/Create', [
            'permissions' => Permission::all(),
        ]);
    }

    public function store(Request $request)
    {

        $this->validate(
            $request,
            [
                'name'      => 'required|string|max:25|unique:roles',
                'permissions' => 'required'
            ],
            [
                'name.required'         => 'Role harus di isi.',
                'name.unique'           => 'Role sudah terpakai.',
                'permissions.required'  => 'Nama Permission harus di isi.',
            ]
        );

        try {

            $role = Role::create([
                'name' => $request->name,
                'guard_name' => 'web',
            ]);

            if ($request->has('permissions')) {
                $role->givePermissionTo(collect($request->permissions)->pluck('id')->toArray());
            }

            return Redirect::route('role')->with('success', 'Success Role & Permissions created');
        } catch (\Exception $e) {
            return Redirect::route('role')->with('error', $e->getMessage());
        }
    }

    public function edit(Role $role)
    {
        return Inertia::render('Admin/Role/Edit', [
            'role'          => $role,
            'permissions'   => Permission::all(),
            'hasPermissions' => $role->permissions,

        ]);
    }

    public function update(Request $request, Role $role)
    {

        $this->validate(
            $request,
            [
                'name'      => 'required|string|max:25',
                'permissions' => 'required'
            ],
            [
                'name.required'         => 'Nama Pengguna harus di isi.',
                'permissions.required'  => 'Nama Permission harus di isi.',
            ]
        );

        try {

            if ($request->has('permissions')) {
                $role->givePermissionTo(collect($request->permissions)->pluck('id')->toArray());
            }

            $role->syncPermissions(collect($request->permissions)->pluck('id')->toArray());
            $role->update(['name' => $request->name]);

            return Redirect::back()->with('success', 'Success Role & Permissions updated.');
        } catch (\Exception $e) {
            return Redirect::back()->with('error', $e->getMessage());
        }
    }

    public function destroy(Role $role)
    {
        try {

            if ($role) {
                $role->delete();
                return Redirect::back()->with('success', 'Success Role deleted.');
            } else {
                throw new \Exception("Failed Delete Role");
            }
        } catch (\Exception $e) {
            return Redirect::back()->with('error', $e->getMessage());
        }
    }
}
