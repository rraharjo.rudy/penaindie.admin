<?php

namespace App\Http\Controllers;

use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Request;
use Inertia\Inertia;
use Illuminate\Support\Facades\Redirect;

class UserController extends Controller
{

    protected $filterField = 'name';
    protected $filterOrderby = 'asc';
    protected $filterShow = 5;

    public function index()
    {

        if (request()->has(['field', 'orderby'])) {
            $this->filterField = request('field');
            $this->filterOrderby = request('orderby');
        }

        if (request('show')) {
            $this->filterShow = request('show');
        }

        // $Users = $Query->paginate($show);
        $Users = User::orderBy($this->filterField, $this->filterOrderby)
            ->Member(1)
            ->filter(request()->only('search', 'trashed', 'verify'))
            ->paginate($this->filterShow)
            ->withQueryString();


        return Inertia::render('Admin/User/Index', [
            'users'     => $Users,
            'filters'   => request()->all(['search', 'field', 'orderby', 'show', 'verify'])
        ]);
    }

    public function create()
    {
        return Inertia::render('Admin/User/Create', [
            'roles' => Role::all()
        ]);
    }

    public function edit(User $user)
    {
        // dd($user->roles()->pluck('name', 'id'));
        return Inertia::render('Admin/User/Edit', [
            'user' => $user,
            'roles' => Role::all()
        ]);
    }

    public function update(Request $request, User $user)
    {

        $this->validate(
            $request,
            [
                'name'      => 'required|string|max:255',
                'email'     => 'required|string|email|max:255|unique:users,email,' . $user->id,
                // 'password'  => 'required|string',
            ],
            [
                'name.required'     => 'Nama Pengguna harus di isi.',
                'email.required'    => 'Email Pengguna harus di isi.',
                'email.unique'      => 'Email Pengguna sudah terpakai.',
                // 'password.required' => 'password Pengguna harus di isi.'
            ]
        );

        try {

            $user = User::find($user->id);
            $user->name = $request->name;
            $user->email = $request->email;
            if (!is_null($request->password)) {
                $user->password = bcrypt($request->password);
            }
            if ($request->role == 4) {
                $user->is_admin = 0;
            }
            $user->save();

            $role = Role::where('id', $request->role)->first();
            $user->syncRoles($role);
            $user->syncPermissions([$role->permissions]);

            return Redirect::back()->with('success', 'Success User updated.');
        } catch (\Exception $e) {
            return Redirect::back()->with('error', $e->getMessage());
        }
    }

    public function store(Request $request)
    {
        $this->validate(
            $request,
            [
                'name'      => 'required|string|max:255',
                'email'     => 'required|string|email|max:255|unique:users',
                'role'      => 'required',
                'password'  => 'required|string',
            ],
            [
                'name.required'     => 'Nama Pengguna harus di isi.',
                'email.required'    => 'Email Pengguna harus di isi.',
                'role.required'     => 'Role harus di isi.',
                'email.unique'      => 'Email Pengguna sudah terpakai.',
                'email.email'       => 'Email harus berupa alamat email yang valid.',
                'password.required' => 'password Pengguna harus di isi.'
            ]
        );

        try {
            $isAdmin = false;
            $isAdminRole = array(1,2);
            if (in_array($request->role, $isAdminRole)) {
                $isAdmin = true;
            }
            // dd($isAdmin);
            $user = User::create([
                'name' => $request->name,
                'email' => $request->email,
                'password' => bcrypt($request->password),
                'is_admin'  => $isAdmin,
                'email_verified_at' => date('Y-m-d H:i:s')
            ]);

            $role = Role::where('id', $request->role)->first();
            $user->syncRoles($role);
            $user->syncPermissions([$role->permissions]);

            return Redirect::route('user')->with('success', 'Success User created');
        } catch (\Exception $e) {
            return Redirect::route('user')->with('error', $e->getMessage());
        }
    }

    public function destroy(User $user)
    {
        //find AttributeCode by ID
        $user = User::findOrfail($user->id);

        //delete AttributeCode
        $user->delete();

        if ($user) {
            // return Redirect::route('user.index')->with('success', 'Success User deleted!');
            return Redirect::back()->with('success', 'Success User deleted.');
        }
    }
}
