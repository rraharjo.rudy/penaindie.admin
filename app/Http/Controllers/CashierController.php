<?php

namespace App\Http\Controllers;

use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Inertia\Inertia;

class CashierController extends Controller
{
    protected $filterField = 'name';
    protected $filterOrderby = 'asc';
    protected $filterSelect = 'all';
    protected $filterShow = 20;

    public function index()
    {

        if (request()->has(['field', 'orderby'])) {
            $this->filterField = request('field');
            $this->filterOrderby = request('orderby');
        }

        if (request('show')) {
            $this->filterShow = request('show');
        }
        
        $Cashiers = User::orderBy($this->filterField, $this->filterOrderby)
            ->whereHas('roles', function ($q) {
                $q->where("name", "cashier");
            })
            ->filter(request()->only('search', 'verify'))
            ->paginate($this->filterShow)
            ->withQueryString();
            
            // return response()->json($Authors);
        return Inertia::render('Admin/Cashier/Index', [
            'cashiers'  => $Cashiers,
            'filters'   => request()->all(['search', 'field', 'orderby', 'show', 'verify'])
        ]);

    }

    public function create()
    {
        return Inertia::render('Admin/Cashier/Create');
    }

    public function store(Request $request)
    {
        $this->validate(
            $request,
            [
                'name'      => 'required|string|max:255',
                'email'     => 'required|string|email|max:255|unique:users',
                'password'  => 'required|string',
            ],
            [
                'name.required'     => 'Nama Pengguna harus di isi.',
                'email.required'    => 'Email Pengguna harus di isi.',
                'email.unique'      => 'Email Pengguna sudah terpakai.',
                'email.email'       => 'Email harus berupa alamat email yang valid.',
                'password.required' => 'password Pengguna harus di isi.'
            ]
        );

        try {

            $user = User::create([
                'name' => $request->name,
                'email' => $request->email,
                'password' => bcrypt($request->password),
                'email_verified_at' => date('Y-m-d H:i:s')
            ]);

            $role = Role::where('id', 4)->first();
            $user->syncRoles($role);
            $user->syncPermissions([$role->permissions]);

            return Redirect::route('cashier')->with('success', 'Success Cashier created');
        } catch (\Exception $e) {
            return Redirect::route('cashier')->with('error', $e->getMessage());
        }
    }

    public function edit(User $cashier)
    {
        return Inertia::render('Admin/Cashier/Edit', [
            'cashier' => $cashier
        ]);
    }

    public function update(Request $request, User $cashier)
    {

        $this->validate(
            $request,
            [
                'name'      => 'required|string|max:255',
                'email'     => 'required|string|email|max:255|unique:users,email,' . $cashier->id,
                // 'password'  => 'required|string',
            ],
            [
                'name.required'     => 'Nama Pengguna harus di isi.',
                'email.required'    => 'Email Pengguna harus di isi.',
                'email.unique'      => 'Email Pengguna sudah terpakai.',
            ]
        );

        try {

            $cashier->name = $request->name;
            $cashier->email = $request->email;
            if (!is_null($request->password)) {
                $cashier->password = bcrypt($request->password);
            }
            $cashier->save();

            return Redirect::back()->with('success', 'Success Cashier updated.');
        } catch (\Exception $e) {
            return Redirect::back()->with('error', $e->getMessage());
        }
    }

    public function destroy(User $cashier)
    {
        if ($cashier) {
            // dd($author);
            $cashier->delete();
            return Redirect::back()->with('success', 'Success Cashier deleted.');
        }
    }

    public function restore($id)
    {   
        User::withTrashed()->find($id)->restore();
        return Redirect::back()->with('success', 'Success Cashier Restore.');        
    }

    
}
