<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Menu;
use Illuminate\Http\Request;
use Inertia\Inertia;
use Illuminate\Support\Facades\Auth;
use Image;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Redirect;

use function GuzzleHttp\Promise\all;

class MenuController extends Controller
{
    protected $filterField = 'name';
    protected $filterOrderby = 'asc';
    protected $filterSelect = 'all';
    protected $filterShow = 5;

    public function index()
    {

        if (request()->has(['field', 'orderby'])) {
            $this->filterField = request('field');
            $this->filterOrderby = request('orderby');
        }

        if (request('show')) {
            $this->filterShow = request('show');
        }
        
        $Menus = Menu::orderBy($this->filterField, $this->filterOrderby)
            ->filter(request()->only('search', 'verify', 'category'))
            ->paginate($this->filterShow)
            ->withQueryString();
        
        return Inertia::render('Admin/Menu/Index', [
            'menus'     => $Menus,
            'categories'=> Category::all(),
            'filters'   => request()->all(['search', 'field', 'orderby', 'show', 'verify', 'category'])
        ]);
    }

    public function create()
    {
        return Inertia::render('Admin/Menu/Create',[
            'categories'=> Category::all(),
        ]);
    }

    public function store(Request $request)
    {
        
        $this->validate(
            $request,
            [
                'name'          => 'required',
                'qty'           => 'required|numeric',
                'price'         => 'required|numeric|min:2',
                'category'      => 'required'
                // 'cover'             => 'mimes:jpeg,png,jpg,gif,svg|max:2048'
            ],
            [
                'name.required'     => 'Nama Awal harus di isi',
                'qty.required'      => 'Qty harus di isi',
                'qty.numeric'       => 'Qty harus di numeric',
                'price.required'    => 'Price harus di isi',
                'price.numeric'     => 'Price harus di numeric',
                'category.required' => 'Category harus di isi',
            ]
        );
        
        try {
            if($request->file('cover')){ 

                $this->validate($request, [ 
                    'cover'   => 'required|image|mimes:jpeg,png,jpg,svg|max:2048',
                ]); 
                
                $fileName = $this->storeImage($request->file('cover'));
               
                $menu = new Menu();
                $menu->user_id = Auth::user()->id;
                $menu->category_id = $request->category["id"];
                $menu->name = $request->name;
                $menu->description = $request->description;
                $menu->qty = $request->qty;
                $menu->price = $request->price;
                $menu->cover = $fileName;
                $menu->save();

            } else {

                $menu = new Menu();
                $menu->user_id = Auth::user()->id;
                $menu->category_id = $request->category["id"];
                $menu->name = $request->name;
                $menu->description = $request->description;
                $menu->qty = $request->qty;
                $menu->price = $request->price;
                $menu->save();
            }
            return Redirect::route('menu')->with('success', 'Success Updated Menu Pengguna ');
        } catch (\Exception $e) {
            return Redirect::back()->with('error', $e->getMessage());
        }
    }

    public function edit(Menu $menu)
    {
        return Inertia::render('Admin/Menu/Edit', [
            'categories'    => Category::all(),
            'menu'          => $menu
        ]);
    }

    public function update(Request $request, Menu $menu)
    {
        
        $this->validate(
            $request,
            [
                'name'          => 'required',
                'qty'           => 'required|numeric',
                'price'         => 'required|numeric|min:2',
                'category'      => 'required'
                // 'cover'             => 'mimes:jpeg,png,jpg,gif,svg|max:2048'
            ],
            [
                'name.required'     => 'Nama Awal harus di isi',
                'qty.required'      => 'Qty harus di isi',
                'qty.numeric'       => 'Qty harus di numeric',
                'price.required'    => 'Price harus di isi',
                'price.numeric'     => 'Price harus di numeric',
                'category.required' => 'Category harus di isi',
            ]
        );

        try {

            if($request->file('cover')){ 
                dd($request->all());
                $this->validate($request, [ 
                    'cover'   => 'required|image|mimes:jpeg,png,jpg,svg|max:2048',
                ]); 

                $fileName = $this->storeImage($request->file('cover'));
               
                $menu->user_id = Auth::user()->id;
                $menu->category_id = $request->category["id"];
                $menu->name = $request->name;
                $menu->description = $request->description;
                $menu->qty = $request->qty;
                $menu->price = $request->price;
                $menu->cover = $fileName;
                $menu->save();

            } else {
                $menu->user_id = Auth::user()->id;
                $menu->category_id = $request->category["id"];
                $menu->name = $request->name;
                $menu->description = $request->description;
                $menu->qty = $request->qty;
                $menu->price = $request->price;
                $menu->save();
            }

            return Redirect::back()->with('success', 'Success Menu updated.');
        } catch (\Exception $e) {
            return Redirect::back()->with('error', $e->getMessage());
        }
    }

    public function storeImage($img)
     {
         $file = $img;
 
         $filenameWithExt = $file->getClientOriginalName();
 
         $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
 
         // Remove unwanted characters
         $filename = preg_replace("/[^A-Za-z0-9 ]/", '', $filename);
         $filename = preg_replace("/\s+/", '-', $filename);
 
         $extension = $file->getClientOriginalExtension();
 
         $fileNameToStore = "menu_" . strtolower($filename).'_'.time().'.'.$extension;
     
         $save = $this->resizeImage($file, $fileNameToStore);
         
         return $save ? $fileNameToStore : "";
     }

     public function resizeImage($file, $fileNameToStore) 
     {
         // Resize image Cover
         $resizeCover = Image::make($file)->resize(1280, null, function ($constraint) {
             $constraint->aspectRatio();
         })->encode('jpg');
 
         // Resize image Thumbnail
         $resizeThumbnail = Image::make($file)->resize(250, 250, function ($constraint) {
             $constraint->aspectRatio();
         })->encode('jpg');
 
         // Put image to storage
         $save = Storage::put("public/images/thumbnail/{$fileNameToStore}", $resizeThumbnail->__toString());
 
         if($save) {
             Storage::put("public/images/{$fileNameToStore}", $resizeCover->__toString());
             return true;
         }
 
         return false;
    }

    

    public function destroy(Menu $menu)
    {
        if ($menu) {
            $menu->delete();
            return Redirect::back()->with('success', 'Success Menu deleted.');
        }
    }

    public function restore($id)
    {   
        Menu::withTrashed()->find($id)->restore();
        return Redirect::back()->with('success', 'Success Menu Restore.');        
    }
}
