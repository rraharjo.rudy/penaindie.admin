<?php

namespace App\Http\Controllers;

use App\Models\Table;
use Illuminate\Http\Request;
use Inertia\Inertia;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Redirect;

class TableController extends Controller
{
    protected $filterField = 'name';
    protected $filterOrderby = 'asc';
    protected $filterSelect = 'all';
    protected $filterShow = 5;

    public function index()
    {

        if (request()->has(['field', 'orderby'])) {
            $this->filterField = request('field');
            $this->filterOrderby = request('orderby');
        }

        if (request('show')) {
            $this->filterShow = request('show');
        }
     
        
        $Tables = Table::orderBy($this->filterField, $this->filterOrderby)
            ->filter(request()->only('search', 'verify'))
            ->paginate($this->filterShow)
            ->withQueryString();
            // return response()->json($Authors);
        return Inertia::render('Admin/Table/Index', [
            'tables'        => $Tables,
            'filters'       => request()->all(['search', 'field', 'orderby', 'show', 'verify'])
        ]);

    }

    public function create()
    {
        return Inertia::render('Admin/Table/Create');
    }

    public function store(Request $request)
    {
        
        $this->validate(
            $request,
            [
                'name'      => 'required|string|max:255|unique:tables,name',
            ],
            [
                'name.required'     => 'Nama Meja harus di isi.',
                'name.unique'       => 'Nama Meja sudah terpakai.',
            ]
        );

        try {
            
            Table::create([
                'name'          => $request->name,
                'slug'          => Str::slug($request->name, '-'),
                'description'   => $request->description,
            ]);

            return Redirect::route('table')->with('success', 'Success Table created');
        } catch (\Exception $e) {
            return Redirect::route('table')->with('error', $e->getMessage());
        }
    }

    public function edit(Table $table)
    {
        return Inertia::render('Admin/Table/Edit', [
            'table' => $table
        ]);
    }

    public function update(Request $request, Table $table)
    {

        $this->validate(
            $request,
            [
                'name'      => 'required|string|max:255|unique:tables,name,' . $table->id,
            ],
            [
                'name.required'     => 'Nama Meja harus di isi.',
                'name.unique'      => 'Nama Meja sudah terpakai.',
            ]
        );

        try {

            $table->name = $request->name;
            $table->slug = Str::slug($request->name, '-');
            $table->description = $request->description;
            $table->save();

            return Redirect::back()->with('success', 'Success Table updated.');
        } catch (\Exception $e) {
            return Redirect::back()->with('error', $e->getMessage());
        }
    }

    public function destroy(Table $table)
    {
        if ($table) {
            $table->delete();
            return Redirect::back()->with('success', 'Success Table deleted.');
        }
    }

    public function restore($id)
    {   
        Table::withTrashed()->find($id)->restore();
        return Redirect::back()->with('success', 'Success Table Restore.');        
    }
}
