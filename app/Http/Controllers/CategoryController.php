<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;
use Inertia\Inertia;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Str;

class CategoryController extends Controller
{
    protected $filterField = 'name';
    protected $filterOrderby = 'asc';
    protected $filterSelect = 'all';
    protected $filterShow = 5;

    public function index()
    {

        if (request()->has(['field', 'orderby'])) {
            $this->filterField = request('field');
            $this->filterOrderby = request('orderby');
        }

        if (request('show')) {
            $this->filterShow = request('show');
        }
     
        
        $Categories = Category::orderBy($this->filterField, $this->filterOrderby)
            ->filter(request()->only('search', 'verify'))
            ->paginate($this->filterShow)
            ->withQueryString();
            
            // return response()->json($Authors);
        return Inertia::render('Admin/Category/Index', [
            'categories'    => $Categories,
            'filters'       => request()->all(['search', 'field', 'orderby', 'show', 'verify'])
        ]);

    }

    public function create()
    {
        return Inertia::render('Admin/Category/Create');
    }

    public function store(Request $request)
    {
        
        $this->validate(
            $request,
            [
                'name'      => 'required|string|max:255|unique:categories,name',
            ],
            [
                'name.required'     => 'Nama Category harus di isi.',
                'name.unique'       => 'Nama Pengguna sudah terpakai.',
            ]
        );

        try {
            
            Category::create([
                'name' => $request->name,
                'slug' => Str::slug($request->name, '-'),
            ]);

            return Redirect::route('category')->with('success', 'Success Category created');
        } catch (\Exception $e) {
            return Redirect::route('category')->with('error', $e->getMessage());
        }
    }

    public function edit(Category $category)
    {
        return Inertia::render('Admin/Category/Edit', [
            'category' => $category
        ]);
    }

    public function update(Request $request, Category $category)
    {

        $this->validate(
            $request,
            [
                'name'      => 'required|string|max:255|unique:categories,name,' . $category->id,
            ],
            [
                'name.required'     => 'Nama Category harus di isi.',
                'email.unique'      => 'Nama Category sudah terpakai.',
            ]
        );

        try {

            $category->name = $request->name;
            $category->slug = Str::slug($request->name, '-');
            $category->save();

            return Redirect::back()->with('success', 'Success Category updated.');
        } catch (\Exception $e) {
            return Redirect::back()->with('error', $e->getMessage());
        }
    }

    public function destroy(Category $category)
    {
        if ($category) {
            $category->delete();
            return Redirect::back()->with('success', 'Success Category deleted.');
        }
    }

    public function restore($id)
    {   
        Category::withTrashed()->find($id)->restore();
        return Redirect::back()->with('success', 'Success Category Restore.');        
    }
}
