<?php

namespace App\Http\Controllers;

use App\Models\Permission;
use Illuminate\Http\Request;
use Inertia\Inertia;
use Illuminate\Support\Facades\Redirect;

class PermissionController extends Controller
{
    public function index()
    {
        return Inertia::render('Admin/Permission/Index', [
            'permissions' => Permission::with('permissions')->paginate(),
        ]);
    }

    public function create()
    {
        return Inertia::render('Admin/Permission/Create');
    }

    public function store(Request $request)
    {

        $this->validate(
            $request,
            [
                'name'          => 'required|string|max:25|unique:permissions',
            ],
            [
                'name.required'         => 'Permissions harus di isi.',
                'name.unique'           => 'Permissions sudah terpakai.',
            ]
        );

        try {

            Permission::create([
                'name'          => $request->name,
                'description'   => $request->description,
                'guard_name'    => 'web',
            ]);

            return Redirect::route('permission')->with('success', 'Success Permission created');
        } catch (\Exception $e) {
            return Redirect::route('permission')->with('error', $e->getMessage());
        }
    }

    public function edit(Permission $permission)
    {
        return Inertia::render('Admin/Permission/Edit', [
            'permission'    => $permission
        ]);
    }

    public function update(Request $request, Permission $permission)
    {

        $this->validate(
            $request,
            [
                'name'      => 'required|string|max:25',
            ],
            [
                'name.required'         => 'Nama Permission harus di isi.',
            ]
        );

        try {

            $permission->update(
                [
                    'name'          => $request->name,
                    'description'   => $request->description
                ]
            );

            return Redirect::back()->with('success', 'Success Permission updated.');
        } catch (\Exception $e) {
            return Redirect::back()->with('error', $e->getMessage());
        }
    }

    public function destroy(Permission $permission)
    {
        try {

            if ($permission) {
                $permission->delete();
                return Redirect::back()->with('success', 'Success Permission deleted.');
            } else {
                throw new \Exception("Failed Delete Permission");
            }
        } catch (\Exception $e) {
            return Redirect::back()->with('error', $e->getMessage());
        }
    }
}
