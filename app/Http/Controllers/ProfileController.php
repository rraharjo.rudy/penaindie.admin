<?php

namespace App\Http\Controllers;

use App\Models\Address;
use App\Models\City;
use App\Models\District;
use App\Models\Province;
use App\Models\User;
use App\Models\Village;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Inertia\Inertia;
use Illuminate\Support\Facades\Redirect;
use Image;
use Illuminate\Support\Facades\Storage;

class ProfileController extends Controller
{
    protected $provinces = [];
    protected $cities = [];
    protected $districts = [];
    protected $villages = [];

    public function index()
    {

        $userAddress = Address::with('Province', 'city', 'district', 'village')
            ->where('user_id', Auth::user()->id)
            ->first();
        // dd($userAddress);
        if (request('province')) {
            // dd(request('province'));
            $this->cities = City::where('province_id', request('province'))->get();
        } else {
            if (!is_null($userAddress)) {
                $this->cities = City::where('province_id', $userAddress->province_id)->get();
            }
        }

        if (request('city')) {
            $this->districts = District::where('city_id', request('city'))->get();
        } else {
            if (!is_null($userAddress)) {
                $this->districts = District::where('city_id', $userAddress->city_id)->get();
            }
        }

        if (request('district')) {
            $this->villages = Village::where('district_id', request('district'))->get();
        } else {
            if (!is_null($userAddress)) {
                $this->villages = Village::where('district_id', $userAddress->district_id)->get();
            }
        }
        
        return Inertia::render('Admin/Profile/Index', [
            'user_address'  => $userAddress,
            'provinces'     => Province::all(),
            'cities'        => $this->cities,
            'districts'     => $this->districts,
            'villages'      => $this->villages,
            'filters'       => request()->all(['province', 'city', 'district'])
        ]);
    }

    public function store(Request $request)
    {
        // dd($request->file('photo'));
       
        $this->validate(
            $request,
            [
                'firstname'         => 'required|alpha',
                'province_id'       => 'required',
                'city_id'           => 'required',
                'district_id'       => 'required',
                'village_id'        => 'required',
                'current_address'   => 'required',
                'permanent_address' => 'required',
                'postal_code'       => 'required',
                'mobile_phone'      => 'required',
                // 'photo'             => 'mimes:jpeg,png,jpg,gif,svg|max:2048'
            ],
            [
                'firstname.required'            => 'Nama Awal harus di isi',
                'firstname.alpha'               => 'Hanya boleh berisi huruf',
                'province_id.required'          => 'Propinsi harus di isi',
                'city_id.required'              => 'Kota harus di isi',
                'district_id.required'          => 'Kelurahan harus di isi',
                'village_id.required'           => 'Kecamatan harus di isi',
                'current_address.required'      => 'Alamat Saat ini harus di isi',
                'permanent_address.required'    => 'Alamat Tetap harus di isi',
                'postal_code.required'          => 'Kode Pos harus di isi',
                'mobile_phone.required'         => 'No Handphone harus di isi',
                'photo.max'                     => 'Foto tidak boleh lebih besar dari 2048 kilobyte.',
                'photo.mimes'                   => 'Foto harus berupa file jenis: jpeg, png, jpg, gif, svg.'
            ]
        );
        // $oldPhoto = Address::where("user_id", "=", Auth::user()->id)->firstOrFail();
        // $fileName = $oldPhoto;
        // dd($oldPhoto);
        try {
            if($request->file('photo')){ 
                $this->validate($request, [ 
                    'photo'   => 'required|image|mimes:jpeg,png,jpg,svg|max:2048',
                ]);
                $fileName = $this->storeImage($request->file('photo'));

                Address::updateOrCreate(
                    ['user_id' => Auth::user()->id],
                    [
                        'fullname'  => $request->firstname . ' ' . $request->lastname,
                        'firstname' => $request->firstname,
                        'lastname' => $request->lastname,
                        'mobile_phone' => $request->mobile_phone,
                        'current_address' => $request->current_address,
                        'permanent_address' => $request->permanent_address,
                        'postal_code' => $request->postal_code,
                        'province_id' => $request->province_id["id"],
                        'city_id' => $request->city_id["id"],
                        'district_id' => $request->district_id["id"],
                        'village_id' => $request->village_id["id"],
                        'photo'     => $fileName,
                        'updated_at'    => date('Y-m-d H:i:s')
                    ]
                );
            } else {
                Address::updateOrCreate(
                    ['user_id' => Auth::user()->id],
                    [
                        'fullname'  => $request->firstname . ' ' . $request->lastname,
                        'firstname' => $request->firstname,
                        'lastname' => $request->lastname,
                        'mobile_phone' => $request->mobile_phone,
                        'current_address' => $request->current_address,
                        'permanent_address' => $request->permanent_address,
                        'postal_code' => $request->postal_code,
                        'province_id' => $request->province_id["id"],
                        'city_id' => $request->city_id["id"],
                        'district_id' => $request->district_id["id"],
                        'village_id' => $request->village_id["id"],
                        'updated_at'    => date('Y-m-d H:i:s')
                    ]
                );
            }
            return Redirect::route('profile')->with('success', 'Success Updated User Pengguna ');
        } catch (\Exception $e) {
            return Redirect::route('profile')->with('error', $e->getMessage());
        }
    }

    public function storeImage($img)
     {
         $file = $img;
 
         $filenameWithExt = $file->getClientOriginalName();
 
         $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
 
         // Remove unwanted characters
         $filename = preg_replace("/[^A-Za-z0-9 ]/", '', $filename);
         $filename = preg_replace("/\s+/", '-', $filename);
 
         $extension = $file->getClientOriginalExtension();
 
         $fileNameToStore = "avatar_" . strtolower($filename).'_'.time().'.'.$extension;
     
         $save = $this->resizeImage($file, $fileNameToStore);
         
         return $save ? $fileNameToStore : "";
     }

     public function resizeImage($file, $fileNameToStore) 
     {
         // Resize image Cover
         $resizeCover = Image::make($file)->resize(1280, null, function ($constraint) {
             $constraint->aspectRatio();
         })->encode('jpg');
 
         // Resize image Thumbnail
         $resizeThumbnail = Image::make($file)->resize(250, 250, function ($constraint) {
             $constraint->aspectRatio();
         })->encode('jpg');
 
         // Create hash value
         $hash = md5($resizeThumbnail->__toString());
 
         // Prepare qualified image name
         $image = "avatar_" . $hash."jpg";
 
         // Put image to storage
         $save = Storage::put("public/images/thumbnail/{$fileNameToStore}", $resizeThumbnail->__toString());
 
         if($save) {
             Storage::put("public/images/{$fileNameToStore}", $resizeCover->__toString());
             return true;
         }
 
         return false;
     }
}
