<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Menu;
use App\Models\Order;
use App\Models\OrderDetail;
use App\Models\Table;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Inertia\Inertia;
use Illuminate\Support\Facades\Redirect;

class OrderController extends Controller
{
    protected $filterField = 'name';
    protected $filterOrderby = 'asc';
    protected $filterSelect = 'all';
    protected $filterShow = 5;

    public function index()
    {
        if (request()->has(['field', 'orderby'])) {
            $this->filterField = request('field');
            $this->filterOrderby = request('orderby');
        }

        if (request('show')) {
            $this->filterShow = request('show');
        }

        $Orders = Order::orderBy($this->filterField, $this->filterOrderby)
            ->filter(request()->only('search', 'verify', 'category'))
            ->paginate($this->filterShow)
            ->withQueryString();

        return Inertia::render('Admin/Order/Index', [
            'orders'     => $Orders,
            'filters'   => request()->all(['search', 'field', 'orderby', 'show', 'verify'])
        ]);
    }

    public function create()
    {
        // dd(Menu::with('category')->get());

        $menus = Menu::leftJoin("categories", "menus.category_id", "=", "categories.id")
            ->select([
                'menus.id',
                'menus.name',
                'menus.cover',
                'menus.qty',
                'menus.price',
                'categories.name as categoryName',
            ])->paginate(6);

        // dd($menus);
        return Inertia::render('Admin/Order/Create', [
            'categories' => Category::all(),
            'menus'     => $menus,
            'tables'    => Table::all(),
        ]);
    }

    public function store(Request $request)
    {
        
        $this->validate(
            $request,
            [
                'customer_name' => 'required',
                'type_payment' => 'required',
                // 'table_order'      => 'required'
            ],
            [
                'customer_name.required' => 'Isian Customer Name wajib diisi',
                'type_payment.required' => 'Isian Type Payment  wajib diisi',
                // 'table_order.required' => 'Isian Meja  wajib diisi',
            ]
        );

        try {
            $order = new Order();
            $order->user_id = Auth::user()->id;
            $order->no_order = '-';
            $order->table_id = $request->table_order;
            $order->customer_name = $request->customer_name;
            $order->customer_phone = $request->customer_phone;
            $order->notes = $request->notes;
            $order->type_payment = $request->type_payment;
            $order->ppn_value = $request->ppn;
            $order->charge_value = $request->charge_value;
            $order->payment_value = $request->payment_value;
            $order->change_value = $request->change_value;
            $order->save();
            $order->no_order = 'PSN' . date('Ymd') . '-' . sprintf('%03d', $order->id);
            $order->update();

            if (count($request->cartItems) > 0) {
                foreach ($request->cartItems as $item) {
                    $orderDetail = new OrderDetail();
                    $orderDetail->order_id = $order->id;
                    $orderDetail->menu_id = $item["id"];
                    $orderDetail->qty = $item["qty"];
                    $orderDetail->total = $item["total"];
                    $orderDetail->save();
                }
            }

            return Redirect::route('order.invoice', ["id" => $order->id])->with('success', 'Success Payment');
        } catch (\Exception $e) {
            return Redirect::route('order.create')->with('error', $e->getMessage());
        }
    }

    public function invoice($id = null)
    {   
        $order = Order::leftJoin("users", "orders.user_id", "=", "users.id")
            ->select([
                'orders.id',
                'orders.no_order',
                'orders.customer_name',
                'orders.customer_phone',
                'orders.notes',
                'orders.type_payment',
                'orders.ppn_value',
                'orders.charge_value',
                'orders.payment_value',
                'orders.change_value',
                'orders.created_at',
                'users.name as cashier',
            ])->where("orders.id", "=", $id)->first();

        $detail = OrderDetail::leftJoin("menus", "order_details.menu_id", "=", "menus.id")
            ->select([
                'order_details.id',
                'order_details.qty',
                'order_details.total',
                'menus.name as menu_name',
                'menus.price as menu_price',
            ])->where("order_details.order_id", "=", $id)->get();

        // $order = Order::find($id);
        return Inertia::render('Admin/Order/Invoice', [
            'order' => $order,
            'detail'=> $detail,
        ]);
    }
}
